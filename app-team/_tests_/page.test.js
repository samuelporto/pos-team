import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom'
import Home from '../src/app/page'

test('renders main content', () => {
    const { getByText } = render(<Home />);
    const linkElement = getByText(/Get started by editing/i);
    expect(linkElement).toBeInTheDocument();
});

test('renders Next.js logo', () => {
    const { getByAltText } = render(<Home />);
    const logoElement = getByAltText('Next.js Logo');
    expect(logoElement).toBeInTheDocument();
});
